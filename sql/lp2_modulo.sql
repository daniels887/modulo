-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 22-Jun-2019 às 21:51
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp2_modulo`
--
CREATE DATABASE IF NOT EXISTS `lp2_modulo` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `lp2_modulo`;
-- --------------------------------------------------------

--
-- Estrutura da tabela `fluxo_relatorio`
--

CREATE TABLE `fluxo_relatorio` (
  `id` int(11) NOT NULL,
  `nome_empresa` varchar(255) NOT NULL,
  `saldo_inicial` double NOT NULL,
  `total_entrada` double NOT NULL,
  `total_saida` double NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `fluxo_relatorio`
--

INSERT INTO `fluxo_relatorio` (`id`, `nome_empresa`, `saldo_inicial`, `total_entrada`, `total_saida`, `last_modified`) VALUES
(1, 'APPLE', 15, 14552220, 10000222, '2019-06-22 19:41:21'),
(3, 'IBM', 1530000, 50000, 20000, '2019-06-22 19:35:11'),
(4, 'MICROSOFT', 2000000000, 1000, 50000, '2019-06-22 19:35:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fluxo_relatorio`
--
ALTER TABLE `fluxo_relatorio`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fluxo_relatorio`
--
ALTER TABLE `fluxo_relatorio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
