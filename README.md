#O Módulo
Fluxo de Caixa com base na planilha do Sebrae

##Configuração
	Clone este repositório em um diretório chamado "modulo";
	Execute em seu phpmyadmin o arquivo "lp2_modulo.sql" que pode ser encontrado na pasta "sql";
	Entre na URL: http://localhost/modulo/fluxo



#### Como utilizar o módulo
	Ao entrar na url acima mencionada,  será visualizada uma tabela com alguns fluxos de caixa como exemplo;
	Na mesma página há a possibilidade de criar um novo fluxo, clicando em "Cadastrar";
	Em cada linha da tabela há as opções de deletar e alterar o fluxo da empresa,
	Clicando sobre a linha da tabela podemos entrar em outra página, onde se localiza um gráfico detalhando visualmente como foi o fluxo de caixa da empresa, no gráfico existe a opção de filtrar os valores que você deseja visualizar, somente clique em cima da legenda desejada.

####Testes realizados no módulo (Fluxo de Caixa)
	Foi criado um teste de integração (onde abrange a interação com o Mysql) para o fluxo, o mesmo se encontra em: http://localhost/modulo/fluxo/test/FluxoTestIntegration
	Para executar o teste de unidade do módulo acesse: http://localhost/modulo/fluxo/test/FluxoTestUnit
	Para executar o teste de regressão acesse: http://localhost/modulo/fluxo/test/all
	Há também mais um teste de limpeza de tabla, acesse: http://localhost/modulo/fluxo/E2ETest

###Feito por
*Daniel de Sousa Pereira - GU3002438*
