<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<div class="container d-flex flex-column align-items-center">
    <div class="row">
        <div class="col-12">
            <h1 class="pt-5 text-primary">Fluxo de Caixa (<?= $nome_empresa ?>)</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div style="width: 400px;height:400px;margin-top:100px;">
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
        </div>
    </div>
</div>
<script>
var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['Saldo Inicial', 'Total de entrada', 'Total de Saída', 'Resultado Operacional', 'Saldo Final'],
        datasets: [{
            data: [<?= $saldo_inicial ?>, <?= $total_entrada ?>, '<?= $total_saida ?>',
                '<?= $total_entrada - $total_saida ?>',
                '<?= $total_entrada - $total_saida + $saldo_inicial ?>'
            ],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>