<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-6">
            <form class="text-center p-5" method="POST">
                <p class="h4 mb-4">Atualizar fluxo de caixa</p>
                <input type="text" id="nome_empresa" name="nome_empresa" class="form-control mb-4" placeholder="Nome da empresa" value="<?= $nome_empresa ?>">
                <input type="text" id="saldo_inicial" name="saldo_inicial" class="form-control mb-4" placeholder="Saldo Inicial" value="<?= $saldo_inicial ?>">
                <input type="text" id="total_entrada" name="total_entrada" class="form-control mb-4" placeholder="Total de entradas" value="<?= $total_entrada ?>">
                <input type="text" id="total_saida" name="total_saida" class="form-control mb-4" placeholder="Total de saídas" value="<?= $total_saida ?>">
                <button class="btn btn-dark btn-block" type="submit">Atualizar</button>
            </form>
        </div>
    </div>
</div>
