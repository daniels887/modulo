<div class="container">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nome da Empresa</th>
                <th scope="col">Saldo Inicial</th>
                <th scope="col">Total de entrada</th>
                <th scope="col">Total de saída</th>
                <th scope="col">Resultado Operacional</th>
                <th scope="col">Saldo Final do Caixa</th>
                <th scope="col"></th>
                <th scope="col"></th>

            </tr>
        </thead>
        <tbody>
            <?php foreach ($resultados as $resultado): ?>
            <tr data-href="<?= base_url('fluxo/grafico/'.$resultado->id) ?>" style="cursor:pointer;">
                <th scope="row"><?= $resultado->id ?></th>
                <td><?= $resultado->nome_empresa ?></td>
                <td>R$ <?= number_format($resultado->saldo_inicial,2,",","."); ?></td>
                <td>R$ <?= number_format($resultado->total_entrada,2,",","."); ?></td>
                <td>R$ <?= number_format($resultado->total_saida,2,",","."); ?></td>
                <td>R$ <?= number_format($resultado->total_entrada - $resultado->total_saida,2,",","."); ?></td>
                <td>R$ <?= number_format($resultado->total_entrada - $resultado->total_saida + $resultado->saldo_inicial,2,",","."); ?></td>
                <td><a href="<?= base_url('fluxo/delete_fluxo/'.$resultado->id) ?>"><i class="fas fa-trash"></i></a>
                </td>
                <td><a href="<?= base_url('fluxo/alterar_fluxo/'.$resultado->id)?>"><i
                            class="fas fa-pencil-alt"></i></a></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="d-flex justify-content-center">
        <a href="<?= base_url('fluxo/cria_fluxo')?>" class="btn btn-primary">Cadastrar</a>
    </div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", () => {
        const rows = document.querySelectorAll('tr[data-href]');

        rows.forEach(row => {
            row.addEventListener("click", () => {
                window.location.href = row.dataset.href;
            })
        })
    })
</script>