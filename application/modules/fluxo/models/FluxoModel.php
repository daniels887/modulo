<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class FluxoModel extends CI_Model{

    /**
     * Gera a tabela que contém a lista de turmas
     */
    public function lista_fluxo(){
        $sql = "SELECT * FROM fluxo_relatorio";
        $res = $this->db->query($sql);
        $data = $res->result();
        return $data; 
    }
    public function get_detalhe($id){
        $sql = "SELECT * FROM fluxo_relatorio WHERE id='$id'";
        $res = $this->db->query($sql);
        $data = $res->result_array();
        return $data[0];
    }
    public function novo_fluxo(){
        $nome_empresa = $this->input->post('nome_empresa');
        $saldo_inicial = $this->input->post('saldo_inicial');
        $total_entrada = $this->input->post('total_entrada');
        $total_saida = $this->input->post('total_saida');

        if(sizeof($_POST) == 0) return;
		else{
            
            $data = array("nome_empresa" => $nome_empresa, "saldo_inicial" => $saldo_inicial, "total_entrada" => $total_entrada,
            "total_saida" => $total_saida);
            $this->db->insert('fluxo_relatorio', $data);
            echo '<script>alert("Fluxo inserido com sucesso!")</script>';
            redirect(base_url('fluxo'));
		}

    }
    public function editar_fluxo($id){
        if($_POST){
                $this->db->set('nome_empresa', $this->input->post('nome_empresa'));    
                $this->db->set('saldo_inicial', $this->input->post('saldo_inicial')); 
                $this->db->set('total_entrada', $this->input->post('total_entrada'));   
                $this->db->set('total_saida', $this->input->post('total_saida'));
                $this->db->where('id', $id);
                $this->db->update('fluxo_relatorio');
                echo '<script>alert("Fluxo Alterado com sucesso!")</script>';
                redirect(base_url('fluxo'));

        }
    }
    public function delete_fluxo($id) {
        $this->db->where('id', $id);
        return $this->db->delete('fluxo_relatorio');

    }
}