<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Fluxo extends Dao {

    function __construct(){
        parent::__construct('fluxo_relatorio');
    }

    public function insert($data, $table = null) {
        // mais uma camada de segurança... além da validação
        $cols = array('nome_empresa', 'saldo_inicial', 'total_entrada', 'total_saida');
        $this->expected_cols($cols);

        return parent::insert($data);
    }

    public function lista(){
        $this->load->model('FluxoModel', 'model');
        return $this->model->lista_fluxo();
    }
}