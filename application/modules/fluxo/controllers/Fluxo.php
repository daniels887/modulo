<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Fluxo extends MY_Controller{
    public function __construct(){
        $this->load->model('FluxoModel', 'model');
    }

    public function index(){
        $this->load->view('config.php');
		$data['resultados'] = $this->model->lista_fluxo();
		$v['tabela'] = $this->load->view('table_fluxo.php', $data, true);
        $this->load->view('listar_fluxo.php', $v);
        $this->load->view('config-footer.php');
    }
    public function cria_fluxo(){
        $this->load->view('config.php');
        $this->model->novo_fluxo();
        $this->load->view('form_fluxo.php');
        $this->load->view('config-footer.php');
    }
    public function alterar_fluxo($id){
        $this->load->view('config.php');
        
        $data = $this->model->get_detalhe($id);
        $this->model->editar_fluxo($id);
        $this->load->view('form_altera_fluxo.php', $data);
        $this->load->view('config-footer.php');
    }
    public function delete_fluxo($id){
        if ($this->model->delete_fluxo($id)) {
                redirect(base_url('fluxo'));
                
        } else {
                log_message('error', 'Erro ao deletar...');
        }
    }
    public function grafico($id) {
        $this->load->view('config.php');
        $data = $this->model->get_detalhe($id);
        $this->load->view('graficos.php', $data);
        $this->load->view('config-footer.php');
    }

}