<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/fluxo/libraries/Fluxo.php';
include_once APPPATH . 'modules/fluxo/controllers/test/builder/FluxoDataBuilder.php';

class E2ETest extends Toast{

    function __construct(){
        parent::__construct('E2E Test');
    }

    function test_limpa_tabela_de_teste(){
        $builder = new FluxoDataBuilder('lp2_modulo');
        $builder->clean_table();

        $tarefa = new Fluxo();
        $data = $tarefa->get();
        $this->_assert_equals_strict(0, sizeof($data), 'Erro na limpeza da tabela');
    }

}