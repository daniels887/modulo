<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/fluxo/libraries/Fluxo.php';
include_once APPPATH . 'modules/fluxo/controllers/test/builder/FluxoDataBuilder.php';

class FluxoTestIntegration extends Toast{
    private $builder;
    private $fluxo;

    function __construct(){
        parent::__construct('FluxoTestIntegration');
    }

    function _pre(){
        $this->fluxo = new Fluxo();
        $this->builder = new FluxoDataBuilder();
    }

    // apenas ilustrativo
    function test_objetos_criados_corretamente(){
        $this->_assert_true($this->builder, "Erro na criação do builder");
        $this->_assert_true($this->fluxo, "Erro na criação do fluxo");
    }

    function test_insere_registro_na_tabela(){
        // cenário 1: vetor com dados corretos
        $this->builder->clean_table();
        $data = $this->builder->getData(0);
        $id1 = $this->fluxo->insert($data);
        $this->_assert_equals(1, $id1, "Esperado 1, recebido $id1");

        // verificação: o objeto criado é, de fato, aquele que enviamos?
        $task = $this->fluxo->get(array('id' => 1))[0];
        $this->_assert_equals($data['nome_empresa'], $task['nome_empresa']);
        $this->_assert_equals($data['saldo_inicial'], $task['saldo_inicial']);
        $this->_assert_equals($data['total_entrada'], $task['total_entrada']);
        $this->_assert_equals($data['total_saida'], $task['total_saida']);

        // cenário 2: vetor vazio
        $id2 = $this->fluxo->insert(array());
        $this->_assert_equals(-1, $id2, "Esperado -1, recebido $id2");

        // cenário 3: vetor com dados inesperados
        $info = $this->builder->getData(1);
        $info['unexpected_col_name'] = 1;
        $id = $this->fluxo->insert($info);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");

        // cenário 4: vetor com dados incompletos... deve ser
        // tratado pela validação, mas tem que ser pensado aqui
        $v = array('titulo' => 'vetor incompleto');
        $id = $this->fluxo->insert($v);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");
    }

    function test_carrega_todos_os_registros_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        $tasks = $this->fluxo->get();
        $this->_assert_equals(4, sizeof($tasks), "Número de registros incorreto");
    }

    function test_carrega_registro_condicionalmente(){
        $this->builder->clean_table();
        $this->builder->build();

        $task = $this->fluxo->get(array('nome_empresa' => 'BWCA', 'id' => 2))[0];
        $this->_assert_equals('BWCA', $task['nome_empresa'], "Erro no nome da empresa");
        $this->_assert_equals(2, $task['id'], "Erro no id do fluxo");
    }
    
    function test_atualiza_registro(){
        $this->builder->clean_table();
        $this->builder->build();

        // lê um registro do bd
        $task1 = $this->fluxo->get(array('id' => 2))[0];
        $this->_assert_equals('BWCA', $task1['nome_empresa'], "Erro no nome da empresa");
        $this->_assert_equals(15000, $task1['saldo_inicial'], "Erro no id do fluxo");

        // atualiza seus valores
        $task1['nome_empresa'] = 'BWCAA';
        $task1['saldo_inicial'] = 1000;
        $this->fluxo->insert_or_update($task1);

        // lê novamente, o mesmo objeto, e verifica se foi atualizado
        $task2 = $this->fluxo->get(array('id' => 2))[0];
        $this->_assert_equals($task1['nome_empresa'], $task2['nome_empresa'], "Erro no nome da empresa");
        $this->_assert_equals($task1['saldo_inicial'], $task2['saldo_inicial'], "Erro no saldo_inicial");
    }
    
    function test_remove_registro_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        // verifica que o registro existe
        $task1 = $this->fluxo->get(array('id' => 2))[0];
        $this->_assert_equals('BWCA', $task1['nome_empresa'], "Erro no nome da empresa");

        // remove o registro
        $this->fluxo->delete(array('id' => 2));

        // verifica que o registro não existe mais
        $task2 = $this->fluxo->get(array('id' => 2));
        $this->_assert_equals_strict(0, sizeof($task2), "Registro não foi removido");
    }
}