<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/fluxo/libraries/Fluxo.php';

class FluxoTestUnit extends Toast{
    private $fluxo;

    function __construct(){
        parent::__construct('FluxoTestUnit');
    }

    function _pre(){
        $this->fluxo = new Fluxo();
    }

    function test_carrega_lista_de_fluxos(){
        $v = $this->fluxo->lista();
        $this->_assert_equals(3, sizeof($v), "Número de fluxos incorreto");
    }

    function test_gera_nome_dos_fluxos(){
        $v = $this->fluxo->lista();
        $this->_assert_equals('APPLE', $v[0]->nome_empresa, "Erro no nome da empresa");
    }

    function test_calculo_resultado_operacional() {
        $v = $this->fluxo->lista();
        $resultado_operacional = $v[1]->total_entrada - $v[1]->total_saida;
        $this->_assert_equals($resultado_operacional, 30000, "Erro no cálculo do resultado operacional");
    }

    function test_calculo_saldo_final() {
        $v = $this->fluxo->lista();
        $resultado_operacional = $v[1]->saldo_inicial + 30000;
        $this->_assert_equals($resultado_operacional, 1560000, "Erro no cálculo do saldo final");
    }

}