<?php
include_once APPPATH.'controllers/test/builder/TestDataBuilder.php';

class FluxoDataBuilder extends TestDataBuilder {

    public function __construct($table = 'lp2_modulo_test'){
        parent::__construct('fluxo_relatorio', $table);
    }

    function getData($index = -1){
        $data[0]['id'] = 1;
        $data[0]['nome_empresa'] = "APPLE";
        $data[0]['saldo_inicial'] = 150000;
        $data[0]['total_entrada'] = 145000;
        $data[0]['total_saida'] = 140000;

        $data[1]['id'] = 2;
        $data[1]['nome_empresa'] = "BWCA";
        $data[1]['saldo_inicial'] = 15000;
        $data[1]['total_entrada'] = 14550;
        $data[1]['total_saida'] = 10000;

        $data[2]['id'] = 3;
        $data[2]['nome_empresa'] = "IBM";
        $data[2]['saldo_inicial'] = 1530000;
        $data[2]['total_entrada'] = 50000;
        $data[2]['total_saida'] = 20000;

        $data[3]['id'] = 4;
        $data[3]['nome_empresa'] = "MICROSOFT";
        $data[3]['saldo_inicial'] = 200000;
        $data[3]['total_entrada'] = 1000;
        $data[3]['total_saida'] = 50000;
        return $index > -1 ? $data[$index] : $data;
    }

}